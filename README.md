# Quantizer

This is a work-in-progress DIY 3-channel CV quantizer module design based on
the ATtiny461A[^1], that is intended to be built on a stripboard.
Its heavily inspired by
[Kassutronics' Quantizer](https://github.com/kassu/kassutronics/tree/master/documentation/Quantizer).


[^1]: The ATtiny461A DIP package version is discontinued. ATtiny861A (and
	ATtiny261A if I can make the code small enough) are still in production in
	DIP and can be used as drop-in replacements.
